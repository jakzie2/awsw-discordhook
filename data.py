loaded = False
player_info = {}
characters = []
char_lookup = {}
char_image_list = []
image_list = []
transforms = []
debug = False


def load(info_data):
    global loaded
    loaded = True
    char_lookup.clear()
    image_list.clear()
    char_image_list.clear()
    player_info.clear()
    characters.clear()
    transforms.clear()

    player_info.update(info_data['player'])
    for name, tag in info_data['characters'].items():
        if tag is None:
            characters.append(name)
            continue
        characters.append(tag)
        char_lookup[tag.lower()] = name
    for image in info_data['images']:
        if image.split(' ', 1)[0].lower() in char_lookup:
            char_image_list.append(image)
        else:
            image_list.append(image)
    transforms.extend(info_data['transforms'])

    characters.sort()
    char_image_list.sort()
    image_list.sort()
    transforms.sort()


def parse_who(who):
    if who.lower() in char_lookup:
        return char_lookup[who.lower()]
    return who


def search_characters(needle):
    return [char for char in characters if needle in char]


def search_char_images(needle):
    return [image for image in char_image_list if needle in image]


def search_images(needle):
    return [image for image in image_list if needle in image]
