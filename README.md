# AwSW Discord Hook

Discord Hook is a program, which allows you to connect your Angels with Scaly Wings game to your Discord server. This allows you and other people of your choice to take control of your game.

This can be used to let other people make an original AwSW adventure on the fly just for you. Showing characters on the screen, talking as them, all from your Discord server.

Read more on the [wiki page](https://gitlab.com/jakzie2/awsw-discordhook/-/wikis/home).

## Installation

The simplest way to get the program running is to download an executable from the wiki page mentioned above.

If you don't trust executables and want to run this program from source, follow these steps:

1. Make sure you have Python 3.10.1 or newer (won't work with 3.10.0 due to a bug in that version).

2. Clone the repository:

```
git clone https://gitlab.com/jakzie2/awsw-discordhook
```

3. Install `pyyaml` and `discord.py` modules:

```
pip install pyyaml git+https://github.com/Rapptz/discord.py
```

4. Now you should be able to launch the program with:

```
python main.py
```

5. If you want to create an executable for the program, you can do it using the `pyinstaller` module:

```
pip install pyinstaller
pyinstaller main.py -F -n DiscordHook
```

6. Now you should find the executable in a newly created `dist` folder.
