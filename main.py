import os
import yaml
from pathlib import Path

import server
import client
import atexit


def main():
    print('AwSW Discord Hook version 0.1.0 - by Jakzie')
    print('-------------------------------------------')

    if os.name == 'nt':
        app_data_folder = os.getenv('APPDATA') + '/awsw_dh/'
    elif os.name == 'posix':
        app_data_folder = os.getenv('HOME') + '/.awsw_dh/'
    else:
        print('This app is not supported on your system.')
        return False

    Path(app_data_folder).mkdir(parents=True, exist_ok=True)

    default_config = {
        'discord_bot_token': 'replace_this_with_bot_token',
        'discord_channel_id': 'replace_this_with_channel_id'
    }

    if os.path.exists(app_data_folder + 'config.yml'):
        with open(app_data_folder + 'config.yml', 'r', encoding='utf-8') as file:
            config = yaml.load(file.read(), yaml.Loader)
    else:
        config = default_config
        with open(app_data_folder + 'config.yml', 'w', encoding='utf-8') as file:
            file.write(yaml.dump(default_config))

    if any(v == default_config[k] for k, v in config.items()):
        print('Please open the configuration file and put your discord bot token and your discord channel id in there.')
        print('You can find the configuration file at: ' + app_data_folder + 'config.yml')

        return False

    atexit.register(on_exit)
    server.start()
    client.run(config['discord_bot_token'], config['discord_channel_id'])

    return True


def on_exit():
    print('Logged off Discord.')
    server.server.stop()
    print('Server stopped.')


if __name__ == '__main__':
    if not main():
        print("\nPress Enter to close.")
        input()
