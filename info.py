import discord


class NextPageButton(discord.ui.Button['NextPageButton']):
    def __init__(self):
        super().__init__(style=discord.ButtonStyle.primary, label='next >')

    async def callback(self, interaction: discord.Interaction):
        await self.view.page.switch(self.view, interaction.message, 1)


class PrevPageButton(discord.ui.Button['PrevPageButton']):
    def __init__(self):
        super().__init__(style=discord.ButtonStyle.primary, label='< prev')

    async def callback(self, interaction: discord.Interaction):
        await self.view.page.switch(self.view, interaction.message, -1)


class PageView(discord.ui.View):
    def __init__(self, page):
        super().__init__()
        self.page = page
        self.add_item(PrevPageButton())
        self.add_item(NextPageButton())


class PageMessage:
    def __init__(self, title, lines):
        self.title = title
        self.pages = []
        self.active_page = 0
        page_count = len(lines) // 20
        for i in range(page_count):
            self.pages.append('\n'.join(lines[i * 20:(i + 1) * 20]))
        if len(lines) % 20 > 0:
            self.pages.append('\n'.join(lines[page_count * 20:]))

    def get_text(self):
        return self.title + ' (Page ' + str(self.active_page + 1) + '/' + str(len(self.pages)) + '):\n' + self.pages[self.active_page]

    async def show(self, channel):
        await channel.send(self.get_text(), view=PageView(self))

    async def switch(self, view, message, count):
        self.active_page = (self.active_page + count) % len(self.pages)
        await message.edit(content=self.get_text(), view=view)

