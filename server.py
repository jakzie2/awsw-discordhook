import threading
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from http.client import HTTPConnection
import client
import data

commands = []


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()

    def do_POST(self):
        post_data = self.rfile.read(int(self.headers['Content-Length']))
        content_str = post_data.decode('utf-8')
        try:
            content = json.loads(content_str)
        except json.decoder.JSONDecodeError:
            self.send_error_response()
            return

        message = 'success'
        if "awsw_magma_client" in content:
            func = content["awsw_magma_client"]

            match func:
                case {"func": "check"}:
                    message = json.dumps({"name": "AwSW Discord Hook", "version": "0.1.0", "min_client_version": '0.0.1'})
                case {'func': 'info_data', 'params': params}:
                    data.load(params)
                    text = 'Connected to player "' + data.player_info['name'] + '".'
                    print(text)
                    client.event(client.e_send_message(text + ' You can now control their game.'))
                case {'func': 'fetch_commands'}:
                    message = json.dumps({"commands": commands})
                    commands.clear()
                case {'func': 'confirm', 'params': {'cmd_id': cmd_id}}:
                    client.event(client.e_confirm_message(cmd_id))
                case {'func': 'cmd_response', 'params': {'cmd_name': 'input', 'cmd_id': cmd_id, 'response': response}}:
                    client.event(client.e_send_response(int(cmd_id), 'Player said: ' + response))
                case {'func': 'cmd_response', 'params': {'cmd_name': 'menu', 'cmd_id': cmd_id, 'response': response}}:
                    client.event(client.e_send_response(int(cmd_id), 'Player selected: ' + response))
                case {'func': 'cmd_error', 'params': {'error_name': 'key_error', 'cmd_name': 'say', 'cmd_id': cmd_id, 'params': {'key': key}}}:
                    client.event(client.e_send_response(int(cmd_id), 'Error: Cannot find character ' + key))
                case {'func': 'cmd_error', 'params': {'error_name': 'key_error', 'cmd_name': 'show', 'cmd_id': cmd_id, 'params': {'key': key}}}:
                    client.event(client.e_send_response(int(cmd_id), 'Error: Invalid transform ' + key))
                case _:
                    message = 'error'
        else:
            message = 'error'

        self.send_response(200)
        self.end_headers()
        self.wfile.write(bytes(message, "utf8"))

    def send_error_response(self):
        self.send_response(400)
        self.end_headers()

    def log_message(self, f, *args):
        if data.debug:
            super().log_message(f, *args)


class Server:
    def __init__(self):
        def run_server():
            while self.running:
                self.server.handle_request()

        self.server = HTTPServer(("localhost", 8042), Handler)
        self.thread = threading.Thread(target=run_server)
        self.thread.daemon = True
        self.running = False

    def stop(self):
        self.running = False
        con = HTTPConnection("localhost", 8042)
        con.request("GET", "/")
        con.close()
        self.thread.join()
        self.server.server_close()


server = Server()


def start():
    server.running = True
    server.thread.start()
