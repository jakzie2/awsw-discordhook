import discord
import server
import data
import info
import asyncio


class Client:
    def __init__(self):
        self.client = discord.Client(intents=discord.Intents(members=True, messages=True, guilds=True))
        self.message_cache = {}
        self.awaits_response = set()
        self.channel_id = 0
        self.event_loop = asyncio.get_event_loop()

        @self.client.event
        async def on_ready():
            print('Logged into Discord as {0.user}.'.format(self.client))
            channel = await self.client.fetch_channel(self.channel_id)
            print('Listening in channel "' + channel.name + '" on server "' + channel.guild.name + '".')
            text = 'Waiting for player to launch MagmaClient.'
            print(text)
            await channel.send('DiscordHook activated. ' + text)

        @self.client.event
        async def on_message(message):
            if message.author == self.client.user or message.author.bot:
                return
            if message.channel.id != self.channel_id:
                return
            if not message.content.startswith('awsw! '):
                return
            if not any(role.name == 'awsw dh' for role in message.author.roles):
                return
            if len(message.content.strip()) == 0:
                return

            cmd = None
            lines = message.content.strip().split('\n')
            if len(lines) <= 1:
                msg_spl = message.content.split()
                if len(msg_spl) <= 1:
                    return
                match msg_spl[1:]:
                    case ['search', *args] if len(args) > 0:
                        await self.search(message.channel, ' '.join(args))
                    case ['list', 'images']:
                        await self.list(message.channel, 'Images', data.image_list)
                    case ['list', 'characters']:
                        await self.list(message.channel, 'Characters', data.characters)
                    case ['list', 'transforms']:
                        await self.list(message.channel, 'Transforms', data.transforms)
                    case ['show', *args] if len(args) > 0:
                        spl = ' '.join(args).split(' at ', 1)
                        cmd = {"name": "show", "params": {"image": spl[0], "at": spl[1] if len(spl) > 1 else 'center'}}
                    case ['hide', *args] if len(args) > 0:
                        cmd = {"name": "hide", "params": {"image": ' '.join(args)}}
                    case ['scene', *args] if len(args) > 0:
                        cmd = {"name": "scene", "params": {"image": ' '.join(args)}}
                    case ['input', *args]:
                        cmd = {"name": "input", "params": {"text": ' '.join(args)}}
                        self.awaits_response.add(message.id)
                    case ['play', channel, *args] if len(args) > 0:
                        cmd = {'name': 'play', 'params': {'channel': channel, 'filename': ' '.join(args)}}
                    case ['stop', channel]:
                        cmd = {'name': 'stop', 'params': {'channel': channel}}
                    case [who, *args] if who[-1] == ':' and len(args) > 0:
                        cmd = {"name": "say", "params": {"who": data.parse_who(who[:-1]), "what": [' '.join(args)]}}
                    case _:
                        await message.channel.send('Invalid command.')
            else:
                msg1_spl = lines[0].split()
                if len(msg1_spl) <= 1:
                    return
                match msg1_spl[1:]:
                    case ['menu:']:
                        cmd = {"name": "menu", "params": {"choices": lines[1:]}}
                    case [who] if who[-1] == ':':
                        cmd = {"name": "say", "params": {"who": data.parse_who(who[:-1]), "what": lines[1:]}}
                    case _:
                        await message.channel.send('Invalid command.')

            if cmd is not None:
                cmd["id"] = str(message.id)
                if data.debug:
                    print(cmd)
                server.commands.append(cmd)
                self.message_cache[message.id] = message
                await message.add_reaction('☑️')

    async def get_message(self, message_id, pop=False):
        if message_id in self.message_cache:
            if pop:
                if message_id in self.awaits_response:
                    self.awaits_response.remove(message_id)
                return self.message_cache.pop(message_id)
            return self.message_cache[message_id]
        channel = await self.client.fetch_channel(self.channel_id)
        return await channel.fetch_message(message_id)

    async def search(self, channel, needle):
        images = ['**Characters:**']
        images.extend(data.search_characters(needle))
        images.append('**Character images:**')
        images.extend(data.search_char_images(needle))
        images.append('**Images:**')
        images.extend(data.search_images(needle))
        await self.list(channel, 'Search results for "' + needle + '"', images)

    async def list(self, channel, title, array):
        if data.loaded:
            await info.PageMessage(title, array).show(channel)
        else:
            await channel.send('This command works only if a player is connected.')


client = Client()


def run(token, channel_id):
    client.channel_id = channel_id
    client.client.run(token)


def event(func_call):
    asyncio.run_coroutine_threadsafe(func_call, client.event_loop)


async def e_confirm_message(message_id):
    try:
        message = await client.get_message(message_id, message_id not in client.awaits_response)
        await message.add_reaction('✅')
    except discord.DiscordException:
        print("Cannot add reaction to message: " + str(message_id))


async def e_send_response(message_id, text):
    message = await client.get_message(message_id, True)
    await message.channel.send(text, reference=message)


async def e_send_message(text):
    channel = await client.client.fetch_channel(client.channel_id)
    await channel.send(text)
